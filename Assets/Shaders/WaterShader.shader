﻿Shader "Custom/WaterShader" {
	Properties {
		_MainTint("Diffuse Tint",Color)=(1,1,1,1)
		_MainTex ("Base (RGB)", 2D) = "white" {}
		_Speed("Speed",Range(0,10))=1
		
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		#pragma surface surf Lambert

		fixed4 _MainTint;
		float _Speed;
		sampler2D _MainTex;

		struct Input {
			float2 uv_MainTex;
		};

		void surf (Input IN, inout SurfaceOutput o) {
			fixed2 scrolledUV=IN.uv_MainTex;
			float rot=_Speed * _Time;
			float xsv=cos(rot*10)/10;
			float ysv=sin(rot);
			scrolledUV+=fixed2(xsv,ysv);
			half4 c=tex2D(_MainTex,scrolledUV);
			o.Albedo=c.rgb*_MainTint;
			o.Alpha=c.a*.5;
		}
		ENDCG
	} 
	FallBack "Diffuse"
	
}
