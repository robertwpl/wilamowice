﻿using UnityEngine;
using System.Collections;

public class LookAtCamera : MonoBehaviour {
    public GameObject target;

    public void LateUpdate()
    {
        transform.LookAt(target.transform);
    }
}
