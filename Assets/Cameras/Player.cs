﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {
    public float movementSpeed = 10f;
    public float turningSpeed = 60f;
	
	void Update () {
        float horizontal = Input.GetAxis("Horizontal") * turningSpeed * Time.deltaTime;
        transform.Rotate(0f, horizontal, 0f);
        float vertical = Input.GetAxis("Vertical")* turningSpeed * Time.deltaTime;
        transform.Translate(0f, 0f, vertical);
	}
}
