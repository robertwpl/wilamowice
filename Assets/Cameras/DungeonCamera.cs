﻿using UnityEngine;
using System.Collections;

public class DungeonCamera : MonoBehaviour {
    public GameObject target;
    Vector3 offset;
    public float damping = 1f;

    public void Start()
    {
        offset = transform.position - target.transform.position;
    }

    public void LateUpdate()
    {
        Vector3 desiredPosition = target.transform.position + offset;
        Vector3 position = Vector3.Lerp(transform.position, desiredPosition, Time.deltaTime * damping);
        transform.position = position;
        transform.LookAt(target.transform.position);
    }
	
}
