﻿using UnityEngine;
using System.Collections;

public class MouseAimCamera : MonoBehaviour {

	public GameObject target;
    Vector3 offset;
    public float rotateSpeed = 5f;
    public void Start()
    {
        offset = target.transform.position - transform.position;
    }

    public void LateUpdate()
    {
        float horizontal = Input.GetAxis("Mouse X") * rotateSpeed;
        target.transform.Rotate(0f, horizontal, 0f);
        float desiredAngle = target.transform.eulerAngles.y;
        Quaternion rotation = Quaternion.Euler(0f, desiredAngle, 0f);
        transform.position = target.transform.position - (rotation * offset);
        transform.LookAt(target.transform);
    }

}
