﻿using UnityEngine;
using System.Collections;

public class FollowCamera : MonoBehaviour {
    public GameObject target;
    Vector3 offset;
    public float damping = 1f;
    public void Start()
    {
        offset = target.transform.position - transform.position;
    }
    public void LateUpdate()
    {
        float currentAngle = transform.eulerAngles.y;
        float desiredAngle = target.transform.eulerAngles.y;
        float angle = Mathf.LerpAngle(currentAngle, desiredAngle, Time.deltaTime * damping);

        Quaternion rotation = Quaternion.Euler(0f, desiredAngle, 0f);
        transform.position = target.transform.position - (rotation * offset);
        transform.LookAt(target.transform);
    }
}
