﻿using UnityEngine;
using System.Collections;

public class CameraOverviewScript : MonoBehaviour {

    private bool sw=true;
    [SerializeField]GameObject TargetImage;
    public void SwitchCamera()
    {
        Camera cam = GetComponent<Camera>();
        gameObject.SetActive(sw);
        TargetImage.SetActive(sw );
        
        sw = !sw;
    }
}
