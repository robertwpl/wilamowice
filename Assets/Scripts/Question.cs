﻿using UnityEngine;
using System.Collections;

public class Question : MonoBehaviour {
    SkinnedMeshRenderer skMeshRend;
    float[] amounts={0f};
    float speed = 2f;
    bool expanding=false;
	void Awake () {
        skMeshRend = transform.FindChild("Rulon").GetComponent<SkinnedMeshRenderer>();
	}

    public void OnCollisionEnter(Collision collision)
    {
        expanding = true;
    }

    public void OnCollisionExit(Collision collision)
    {
        expanding = false;
    }

    public void OnMouseEnter()
    {
        expanding = true;
    }

    public void OnMouseExit()
    {
        expanding = false;
    }

    public void Update()
    {
        Debug.Log(expanding);
        if (expanding && amounts[0] < 100f)
        {
            skMeshRend.SetBlendShapeWeight(0, Mathf.Lerp(skMeshRend.GetBlendShapeWeight(0), amounts[0], Time.time));
            amounts[0] += speed;
        }
        else if (!expanding && amounts[0] > 0f)
        {
            skMeshRend.SetBlendShapeWeight(0, amounts[0]);
            amounts[0] -= speed;
        }
    }
	 
	
    
}
