﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;

public class PlayerJumper : MonoBehaviour, IEndDragHandler
{
    public GameObject Player;
    Slider sl;
    Vector3[] positions=new Vector3[12];

    void Start()
    {
        sl = GetComponent<Slider>();
        positions[1] = new Vector3(2.377618f, 0.1050029f, 26.2343f);//groch
        positions[2] = new Vector3(5.152497f, 0.1049993f, -5.250428f);
positions[3] = new Vector3(24.6102f, 0.1049947f, 23.43362f);//konski
positions[4] = new Vector3(-3.089148f, 0.1049971f, -0.5110054f);//kozuch
positions[5] = new Vector3(0.6939707f, 0.1284093f, -6.893542f);//jozef
positions[6] = new Vector3(-0.4145223f, 0.1049955f, 14.39632f);//na pieklo
positions[7] = new Vector3(7.433718f, 0.1049961f, 7.446968f);//kosciol
positions[8] = new Vector3(-2.781089f, 0.1050004f, -27.69918f);//wojna
positions[10] = new Vector3(1.736545f, 0.008941174f, 17.98436f);//finish - pyramid
        positions[11] = new Vector3(-8.979446f, 0.1050065f, 51.27013f);//step down
    }
 

    public void OnEndDrag(PointerEventData eventData)
    {
        Debug.Log("on drag " + sl.value);
        Vector3 v = positions[(int)sl.value];
        Player.transform.position=new Vector3(v.x, v.y, v.z);
    }
}
