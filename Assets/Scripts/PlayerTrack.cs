﻿using UnityEngine;
using System.Collections;

public class PlayerTrack : MonoBehaviour
{
    public GameObject Player;

    void Update()
    {
        transform.eulerAngles = new Vector3(270f, Player.transform.eulerAngles.y + 180f, transform.eulerAngles.z);
        transform.position = new Vector3(Player.transform.position.x, Player.transform.position.y, Player.transform.position.z);
    }
}
