﻿using UnityEngine;

public class Texts : MonoBehaviour {
    GameObject text;
    [SerializeField]
    PlayerScript Player;

    public void PointerClickSign(GameObject go)
    {
        Debug.Log("Click PointerClickSign " + go.name);
        text= transform.GetChild(transform.FindChild(go.name).GetSiblingIndex()).gameObject;
        text.SetActive(true);
        gameObject.SetActive(true);
        Player.SetActiveMove(false);
    }
    public void ZoneClick()
    {
        text.SetActive(false);
        gameObject.SetActive(false);
        Player.SetActiveMove(true);
    }
}
