﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.Characters.FirstPerson;
public class PlayerScript : MonoBehaviour {

	public void SetActiveMove(bool active)
    {
        GetComponent<FirstPersonController>().gameObject.SetActive(active);
    }
}
